describe("Test suite - conjunto de pruebas", () => {

    beforeEach(() => {
        // root-level hook
        // runs before every test block
        cy.visit("http://zero.webappsecurity.com/")
      })


    it("Validar pagina de inicio", () => {
        
        cy.get(".active > img").should("be.visible")
        cy.get('.active > .custom > h4').contains('Online Banking')
    })

    it("Validar transferencia de fondos", () => {
        
        cy.get('#transfer_funds_link').click()
        cy.get('#user_login').type("username")
        cy.get('#user_password').type("password")
        cy.get('.btn').click()
        cy.get('#tf_fromAccountId').select(1)
        cy.get('#tf_toAccountId').select(2)
        cy.get('#tf_amount').type(250)
        cy.get('#tf_description').type("transferencia de fondos")
        cy.get('#btn_submit').click()
        cy.get('p').should("be.visible")
        cy.get('#btn_submit').click()
        cy.get('.alert').should("be.visible")
    })

    it("Validar la interaccion del mapa de dinero", () => {
       
        cy.get('#signin_button').click()
        cy.get('#user_login').type("username")
        cy.get('#user_password').type("password")
        cy.get('.btn').click()
        cy.get('#money_map_tab > a').click()
        cy.get('#ext-sprite-1263').should("be.visible")
        cy.get('#ext-sprite-1175 > tspan').click()
        cy.get('#ext-sprite-1263').should("not.be.visible")
    })
})